import React, {Component, Fragment} from 'react';
import {fetchHistory} from "../../store/actions/TrackActions";
import {connect} from "react-redux";
import {Card, CardTitle} from "reactstrap";

class TrackHistoryPage extends Component {
  componentDidMount(){
    this.props.fetchHistory()
  }
  render() {
    return (
     <Fragment>
       {this.props.history ? this.props.history.map(item=>(
         <Card key={item._id} style={{marginTop: '20px'}}>
           <CardTitle>
             <b>Название музыки: </b>{item.track.name}

           </CardTitle>
           <CardTitle>
             <b>Длительность: </b>{item.track.lasting}
           </CardTitle>
           <CardTitle>
             <b>Номер компазиции: </b>{item.track.number}
           </CardTitle>
         </Card>

         )):null }
     </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  history: state.tracks.history
});

const mapDispatchToProps = dispatch => ({
  fetchHistory: () => dispatch(fetchHistory())
});

export default connect(mapStateToProps, mapDispatchToProps) (TrackHistoryPage);