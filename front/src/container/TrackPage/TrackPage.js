import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchTrack, postTrack} from "../../store/actions/TrackActions";
import MusicThumbnail from "../../components/MusicThumbnail/MusicThumbnail";
import './TrackPage.css';
import {Card, CardBody, CardColumns, CardTitle, Col} from "reactstrap";

class TrackPage extends Component {
  componentDidMount(){
    this.props.fetchTracks(this.props.match.params.id);
  }
  render() {
    return (
      <Fragment>
        <hr/>
        <h2>Tracks</h2>
        <hr/>
        <div className='album-tracks'>
          {this.props.tracks ? <div>
            <MusicThumbnail image={this.props.tracks[0].album.img}/>
            <h2><b>Название Альбома:</b> {this.props.tracks[0].album.title}</h2>
            <p><b>Описание:</b> {this.props.tracks[0].album.description}</p>
            <p><b>Дата выпуска:</b> {this.props.tracks[0].album.release}</p>
          </div>: null}

        </div>
        <div className='artist-tracks'>
          {this.props.tracks ? <div>
            <MusicThumbnail image={this.props.tracks[0].album.artist.img}/>
            <h2><b>Имя исполнителя:</b> {this.props.tracks[0].album.artist.name}</h2>
            <p><b>Биография: </b>{this.props.tracks[0].album.artist.description}</p>
          </div> : null}
        </div>
        <hr/>
        <Col>
          <CardColumns>
            {this.props.tracks ? this.props.tracks.map(item=>(
              <Card key={item._id} sm={6} onClick={()=>this.props.pushHistory({track: item._id})}>
                <CardTitle>
                  <h3><b>Track: </b>{item.name}</h3>
                </CardTitle>
                <CardBody>
                  <p><b>Lasting: </b>{item.lasting}</p>
                  <span>number: {item.number}</span>
                </CardBody>

              </Card>
            )): null}
          </CardColumns>

        </Col>
      </Fragment>
    );
  }
}
const mapStateToProps = state => {
  return{
    tracks: state.tracks.tracks,
    oneAlbum: state.album.oneAlbum
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchTracks: id => dispatch(fetchTrack(id)),
    pushHistory: (data) => dispatch(postTrack(data))
  }
};

export default connect(mapStateToProps, mapDispatchToProps) (TrackPage);