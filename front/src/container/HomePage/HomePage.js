import React, {Component, Fragment} from 'react';
import {fetchArtist} from "../../store/actions/ArtistActions";
import {connect} from "react-redux";
import MusicThumbnail from '../../components/MusicThumbnail/MusicThumbnail'

import './HomePage.css'
import {Button, NavLink} from "reactstrap";
import {NavLink as RouterLink} from 'react-router-dom';

class HomePage extends Component {
  componentDidMount(){
    this.props.fetchArtist()
  }
  render() {

    return (
      <Fragment>
        <h1 style={{marginTop: '20px', textAlign: 'center'}}>Artist's</h1>
        <hr/>
        {this.props.artist ? this.props.artist.map(item=>(
          <div key={item._id} className='item-card'>
            <MusicThumbnail image={item.img}/>
            <h2 className='item-title'>{item.name}</h2>
            <NavLink tag={RouterLink} to={`/album/${item._id}`}>Read More  >>></NavLink>
            {this.props.user && this.props.user.role === 'admin' ? <Button className='artist-btn' color='danger'>Delete</Button>: null}
          </div>
        )):null}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    artist: state.artist.artist,
    user: state.user.user
  }
};
const mapDispatchToProps = dispatch => {
  return {
    fetchArtist: ()=> dispatch(fetchArtist())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);