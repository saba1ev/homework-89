import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import MusicThumbnail from "../../components/MusicThumbnail/MusicThumbnail";
import {fetchAlbums} from "../../store/actions/AlbumActions";
import './AlbumPage.css'
import {NavLink} from "reactstrap";
import {NavLink as RouterLink} from "react-router-dom";
class AlbumPage extends Component {
  componentDidMount(){
    this.props.fetchAlbums(this.props.match.params.id)
  }

  render() {
    console.log(this.props.album);
    return (
      <Fragment>
        <h1>Album's lol</h1>
        <hr/>

        {this.props.album ? this.props.album.map(item=>(
          <div key={item._id} className='item-album'>
            <MusicThumbnail image={item.img}/>
            <h3>{item.title}</h3>
            <NavLink tag={RouterLink} to={`/tracks/${item._id}`}>go to track >>></NavLink>
          </div>
        )):null}
      </Fragment>
    );
  }
}
const mapStateToProps = state => {
  return{
    album: state.album.album
  }
};
const mapDispatchToProps = dispatch => {
  return {
    fetchAlbums: (id) => dispatch(fetchAlbums(id))
  }
};
export default connect(mapStateToProps, mapDispatchToProps) (AlbumPage);