import {FETCH_ARTIST_FAILURE, FETCH_ARTIST_SUCCESS} from "../actions/ActionsTypes";

const initialState = {
  artist: null,
  error: null
};

const reducer = (state = initialState, action)=>{
  switch (action.type) {
    case FETCH_ARTIST_SUCCESS:
      return{
        ...state,
        artist: action.payload
      };
    case FETCH_ARTIST_FAILURE:
      return{
        ...state,
        error: action.fail
      };
    default:
      return state

  }
};

export default reducer;