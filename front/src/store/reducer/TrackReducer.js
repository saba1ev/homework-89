import {
  FETCH_HISTORY_FAILURE,
  FETCH_HISTORY_SUCCESS,
  FETCH_TRACK_FAILURE,
  FETCH_TRACK_SUCCESS
} from "../actions/ActionsTypes";

const initalState = {
  tracks: null,
  error: null,
  history: null,
  historyError: null
};

const reducer = (state = initalState, action) => {
  switch (action.type) {
    case FETCH_TRACK_SUCCESS:
      return {...state, tracks: action.payload};
    case FETCH_TRACK_FAILURE:
      return {...state, error: action.failure};
    case FETCH_HISTORY_SUCCESS:
      return {...state, history: action.history};
    case FETCH_HISTORY_FAILURE:
      return {...state, historyError: action.failure};
    default:
      return state;
  }
};

export default reducer;