import {FETCH_ALBUM_FAILURE, FETCH_ALBUM_SUCCESS} from "../actions/ActionsTypes";

const initialState = {
  album: null,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALBUM_SUCCESS:
      return {...state, album: action.payload};
    case FETCH_ALBUM_FAILURE:
      return {...state, error: action.fail};
    default:
      return state
  }
};

export default reducer;