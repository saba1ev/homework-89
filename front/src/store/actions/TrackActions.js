import axios from '../../axios-music';
import {push} from 'connected-react-router'
import {
  FETCH_HISTORY_FAILURE,
  FETCH_HISTORY_SUCCESS,
  FETCH_TRACK_FAILURE,
  FETCH_TRACK_SUCCESS,
  POST_HISTORY_SUCCESS
} from "./ActionsTypes";
//get action tracks
export const fetchTrackSuccess = payload => {
  return {type: FETCH_TRACK_SUCCESS, payload}
};
export const fetchTrackFailure = failure => {
  return {type: FETCH_TRACK_FAILURE, failure}
};
//history action post
export const postHistoryTrack = () => {
  return {type: POST_HISTORY_SUCCESS}
};
// history action get
export const fetchHistorySuccess = (history) => {
  return {type: FETCH_HISTORY_SUCCESS, history}
};
export const fetchHistoryFailure = (failure) => {
  return {type: FETCH_HISTORY_FAILURE, failure}
}


export const fetchTrack = id =>{
  return dispatch => {
    axios.get('/track?album=' + id)
      .then(response => dispatch(fetchTrackSuccess(response.data)))
      .catch(error => dispatch(fetchTrackFailure(error)))
  }
};



//Track_History
export const postTrack = (data) =>{
  return (dispatch, getState) => {
    const user = getState().user.user;
    if (user === null){
      dispatch(push('/login'))
    } else {
      return axios.post('/track_history', data, {headers: {'Token': user.token}})
        .then(() => dispatch(postHistoryTrack()))
    }

  }
};


export const fetchHistory = () => {
  return (dispatch, getState) => {
    const user = getState().user.user;
    if (user === null){
      dispatch(push('/login'))
    } else {
      axios.get('/track_history', {headers: {'Token': user.token}})
        .then((response)=> dispatch(fetchHistorySuccess(response.data)))
        .catch(err => dispatch(fetchHistoryFailure(err)))
    }

  }
}