import axios from '../../axios-music';
import {FETCH_ALBUM_FAILURE, FETCH_ALBUM_SUCCESS} from "./ActionsTypes";


export const fetchAlbumSuccess = payload =>{
  return {type: FETCH_ALBUM_SUCCESS, payload}
};
export const fetchAlbumFailure = fail => {
  return {type: FETCH_ALBUM_FAILURE, fail}
};

export const fetchAlbums = (id) =>{
  console.log(id);
  return dispatch => {
    axios.get('/album?artist=' + id)
      .then(response => dispatch(fetchAlbumSuccess(response.data)))
      .catch(error => dispatch(fetchAlbumFailure(error)))
  }
};
