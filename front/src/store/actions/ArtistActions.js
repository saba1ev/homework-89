import {FETCH_ARTIST_FAILURE, FETCH_ARTIST_SUCCESS} from "./ActionsTypes";
import axios from '../../axios-music';

export const fetchArtistSuccess = (payload) =>{
  return {type:FETCH_ARTIST_SUCCESS, payload}
};

export const fetchArtistFailure = (fail) =>{
  return {type: FETCH_ARTIST_FAILURE, fail}
};



export const fetchArtist = () =>{
  return dispatch =>{
    axios.get('/artist')
      .then(response=> dispatch(fetchArtistSuccess(response.data)))
      .catch(erorr => dispatch(fetchArtistFailure(erorr)))
  };
};

