import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import {createBrowserHistory} from "history";
import {connectRouter, routerMiddleware} from "connected-react-router";

import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";

import artistReducer from './reducer/ArtistReducer';
import userReducer from './reducer/UserReducer';
import albumReducer from './reducer/AlbumReducer';
import trackReducer from './reducer/TrackReducer'

export const history = createBrowserHistory();

const rootReducer = combineReducers({
  router: connectRouter(history),
  user: userReducer,
  artist: artistReducer,
  album: albumReducer,
  tracks: trackReducer

});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
  thunkMiddleware,
  routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
  saveToLocalStorage({
    user: {
      user: store.getState().user.user
    }
  });
});

export default store;