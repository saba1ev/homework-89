import React, {Component, Fragment} from 'react';
import {NotificationContainer} from "react-notifications";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {connect} from "react-redux";
import {withRouter} from 'react-router'
import {Route, Switch} from "react-router-dom";
import {logoutUser} from "./store/actions/UserActions";
import {Container} from "reactstrap";
import HomePage from "./container/HomePage/HomePage";
import AlbumPage from "./container/AlbumPage/AlbumPage";
import TrackPage from "./container/TrackPage/TrackPage";
import RegisterPage from "./container/RegisterPage/RegisterPage";
import Login from "./container/Login/Login";
import TrackHistoryPage from "./container/Track_HistoryPage/Track_HistoryPage";

class App extends Component {
  render() {
    return (
      <Fragment>
        <NotificationContainer/>
        <header>
          <Toolbar
            user={this.props.user}
            logout={this.props.logoutUser}
          />
        </header>
        <Container>
          <Switch>
            <Route path="/" exact component={HomePage}/>
            <Route path='/login' exact component={Login}/>
            <Route path='/history' component={TrackHistoryPage}/>
            <Route path='/album/:id' component={AlbumPage}/>
            <Route path='/tracks/:id' component={TrackPage}/>

            <Route path='/register' component={RegisterPage}/>

          </Switch>
        </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps,)(App));