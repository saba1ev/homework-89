import React, {Fragment} from 'react';
import {NavLink as RouterNavLink} from "react-router-dom";
import {DropdownItem, DropdownMenu, DropdownToggle, NavItem, NavLink, UncontrolledDropdown} from "reactstrap";

const UserMenu = ({user, logout}) => (
  <Fragment>
    <NavItem>
      <NavLink tag={RouterNavLink} to="/history" exact>Track History</NavLink>
    </NavItem>
    <UncontrolledDropdown nav inNavbar>

      <DropdownToggle nav caret>
        Hello, {user.nickname}!
      </DropdownToggle>

      <DropdownMenu right>
        <DropdownItem tag={RouterNavLink} to="/artist/new">
          Add new Artist
        </DropdownItem>
        <DropdownItem tag={RouterNavLink} to="/album/new">
          Add new Album
        </DropdownItem>
        <DropdownItem tag={RouterNavLink} to="/track/new">
          Add new Track
        </DropdownItem>
        <DropdownItem divider/>
        <DropdownItem onClick={logout}>
          Logout
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  </Fragment>


);

export default UserMenu