import React from 'react';
import music from '../../assets/images/5d247ce0e496ee12e8293516b4275b49.jpg';
import {apiURL} from "../../constants";


const styles = {
  width: '150px',
  height: '150px',
  marginRight: '10px'
};

const MusicThumbnail = props => {
  let image = music;

  if (props.image) {
    image = apiURL + '/uploads/' + props.image;
  }


  return <img src={image} style={styles} className="img-thumbnail" alt="Post Img"/>
};

export default MusicThumbnail;
