const path = require('path');
const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, "public/uploads"),
  dbURL: 'mongodb://localhost/music',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook:{
    appId: '2271700436276317',
    appSecret: '9b0a00a5fea8962a39da24cc9f49c1bf'
  }
};