const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const Artist = require('./models/ArtistsModels');
const Album = require('./models/AlbumsModels');
const Track = require('./models/TracksModels');
const Users = require('./models/UsersModels');

const run = async () =>{

  await mongoose.connect(config.dbURL, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();
  
  for (let collection of collections) {
    await collection.drop()
  }

  const users = await Users.create(
    {
      username: 'hacker',
      password: 'lol',
      nickname: 'Hunter',
      role: 'admin',
      token: nanoid()
    },
    {
      username: 'test',
      password: 'test',
      nickname: 'bro',
      role: 'user',
      token: nanoid()
    }
  );
  const artists = await Artist.create(
    {
      user: users[0]._id,
      publish: true,
      name: 'Lil Jon',
      description: 'Jonathan Smith (born January 17, 1971), known professionally as Lil Jon, is an American rapper, record producer, and DJ based in Atlanta, Georgia. He was the lead frontman of the multi-platinum selling rap group, Lil Jon & The East Side Boyz and was instrumental in the emergence of the sub hip/hop genre Crunk.',
      img: 'lil.jpg'
    },
    {
      user: users[1]._id,
      publish: true,
      name: 'Scripton',
      description: 'Adil Oralbekovich Jalelov (Kazakh Әdіl Oralbekuly Zhәyolov, born on June 3, 1990, Leninsky, Kazakh Soviet Socialist Republic), better known by the stage name Scriptonite (Kazakh Scriptonite) - Kazakhstani rap artist and beatmaker, member of the creative association Gazglite For the first time he loudly announced himself in 2014 with a video clip for the song “VBVVCTND”, after a year and a half he released his debut album “House with Normal Phenomena”, which became one of the most successful Russian-language rap albums of 2015.',
      img: 'skrip.jpeg'
    },
    {
      user: users[0]._id,
      publish: true,
      name: 'Billy Ray Cyrus',
      description: 'Billy Ray Cyrus (born August 25, 1961) is an American singer, songwriter and actor. Having released 12 studio albums and 44 singles since 1992, he is best known for his number one single "Achy Breaky Heart", which became the first single ever to achieve triple Platinum status in Australia.',
      img: 'billy.png'
    }
  );
  const albums = await Album.create(
    {
      artist: artists[0]._id,
      title: 'We Still Crunk!',
      publish: true,
      user: users[0]._id,
      release: '15.08.2000',
      description: 'We Still Crunk has 17 tracks',
      img: 'crunk.jpg'
    },
    {
      artist: artists[1]._id,
      title: 'Дом с нормальными явлениями',
      publish: true,
      user: users[0]._id,
      release: '05.03.2015',
      description: 'Дом с нормальными явлениями. Имеет 18 трэков в Альбоме',
      img: 'house.jpeg'
    },
    {
      artist: artists[0]._id,
      title: 'Put Yo Hood Up',
      publish: true,
      user: users[0]._id,
      release: '22.05.2001',
      description: 'Put Yo Hood Up has 18 tracks',
      img: 'hood.jpg'
    },
    {
      artist: artists[2]._id,
      title: 'Back to Tennessee',
      publish: true,
      user: users[0]._id,
      release: '07.04.2009',
      description: 'Back to Tennessee has 8 tracks',
      img: 'back.jpg'
    },
    {
      artist: artists[2]._id,
      title: 'Home at Last',
      user: users[0]._id,
      publish: true,
      release: '24.07.2007',
      description: 'Home at Last has 13 tracks',
      img: 'last.jpg'
    },
    {
      artist: artists[1]._id,
      title: 'Праздник на улице 36',
      user: users[0]._id,
      publish: true,
      release: '24.05.2017',
      description: 'Праздник на улице 36. Имеет 13 трэков в альбоме',
      img: 'sk.jpeg'
    }
  );
  const tracks = await Track.create(
    //We Still Crunk! start
    {
      name: 'We Still Crunk',
      publish: true,
      album: albums[0]._id,
      lasting: '1:55',
      number: 1
    },
    {
      name: 'I Like Dem Girlz',
      album: albums[0]._id,
      lasting: '4:30',
      publish: true,
      number: 2
    },
    {
      name: 'Where Dem Girlz At?',
      album: albums[0]._id,
      lasting: '4:00',
      publish: true,
      number: 3
    },
    {
      name: 'Bounce Dat Azz',
      album: albums[0]._id,
      lasting: '4:13',
      publish: true,
      number: 4
    },
    {
      name: 'Let My Nuts Go',
      album: albums[0]._id,
      lasting: '4:48',
      publish: true,
      number: 5
    },
    //We Still Crunk! end
    //Дом с нормальными явлениями start
    {
      name: 'Интро',
      album: albums[1]._id,
      lasting: '1:18',
      publish: true,
      number: 1
    },
    {
      name: 'Дома',
      album: albums[1]._id,
      lasting: '3:32',
      publish: true,
      number: 2
    },
    {
      name: 'Оставь это нам',
      album: albums[1]._id,
      lasting: '1:40',
      publish: true,
      number: 3
    },
    {
      name: 'Вниз',
      album: albums[1]._id,
      lasting: '3:44',
      publish: true,
      number: 4
    },
    {
      name: 'Коньяк',
      album: albums[1]._id,
      lasting: '2:21',
      publish: true,
      number: 5
    },
    //Дом с нормальными явлениями end
    //Put Yo Hood Up start
    {
      name: 'Y\'all Ain\'t Ready',
      album: albums[2]._id,
      lasting: '1:39',
      publish: true,
      number: 1
    },
    {
      name: 'Uhh Ohh',
      publish: true,
      album: albums[2]._id,
      lasting: '5:08',
      number: 2
    },
    {
      name: 'Put Yo Hood Up',
      album: albums[2]._id,
      publish: true,
      lasting: '5:07',
      number: 3
    },
    {
      name: 'Who U Wit',
      album: albums[2]._id,
      lasting: '4:31',
      publish: true,
      number: 4
    },
    {
      name: 'Bia\' Bia\'',
      album: albums[2]._id,
      lasting: '5:01',
      publish: true,
      number: 5
    },
    //Put Yo Hood Up end
    //Back to Tennessee start
    {
      name: 'Back to Tennessee',
      album: albums[3]._id,
      lasting: '4:15',
      publish: true,
      number: 1
    },
    {
      name: 'Thrillbilly',
      album: albums[3]._id,
      lasting: '3:22',
      publish: true,
      number: 2
    },
    {
      name: 'He\'s Mine',
      album: albums[3]._id,
      lasting: '3:44',
      publish: true,
      number: 3
    },
    {
      name: 'Somebody Said a Prayer',
      album: albums[3]._id,
      lasting: '4:04',
      number: 4,
      publish: true,
    },
    {
      name: 'A Good Day',
      album: albums[3]._id,
      lasting: '4:08',
      number: 5,
      publish: true,
    },
    //Back to Tennessee end
    //Home at Last start
    {
      name: 'Lonely Boy',
      album: albums[4]._id,
      lasting: '3:09',
      number: 1,
      publish: true,
    },
    {
      name: 'My Feet Are On The Rock',
      album: albums[4]._id,
      lasting: '4:56',
      number: 2,
      publish: true,
    },
    {
      name: 'Country Church',
      album: albums[4]._id,
      lasting: '4:52',
      number: 3,
      publish: true,
    },
    {
      name: 'Sitting In My Kitchen',
      album: albums[4]._id,
      lasting: '4:31',
      number: 4,
      publish: true,
    },
    {
      name: 'Camel Through A Needle\'s Eye',
      album: albums[4]._id,
      lasting: '5:18',
      number: 5,
      publish: true,
    },
    //Home at Last end
    //Праздник на улице 36 start
    {
      name: 'Капли вниз по бёдрам',
      album: albums[5]._id,
      lasting: '3:52',
      number: 1,
      publish: true,
    },
    {
      name: 'Напомни',
      album: albums[5]._id,
      lasting: '3:22',
      number: 2,
      publish: true,
    },
    {
      name: '104 (Скит)',
      album: albums[5]._id,
      lasting: '1:13',
      number: 3,
      publish: true,
    },
    {
      name: 'Цепи',
      album: albums[5]._id,
      lasting: '3:27',
      number: 4,
      publish: true,
    },
    {
      name: 'Точно не нужно новых',
      album: albums[5]._id,
      lasting: '4:05',
      number: 5,
      publish: true,
    },
    //Праздник на улице 36 end
  );
  return connection.close();

};

run().catch(error => {
  console.error('Something went wrong!', error);
});