const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const artists = require('../models/ArtistsModels');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');

const storage = multer.diskStorage({
  destination: (req, res, cb)=>{
    cb(null, config.uploadPath);
  },
  filename: (req, res, cb) =>{
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});


const router = express.Router();

router.get('/', tryAuth, async (req, res)=>{

  try {
    let criteria = {publish: true};
    if (req.user && req.user.role === 'user'){
      criteria = {
        $or : [
          {publish: false},
          {user: req.user._id}
        ]
      }
    } else if (req.user && req.user.role === 'admin'){
      criteria = {}
    }
    const artist = await artists.find(criteria);

    return res.send(artist);

  } catch (e) {
    if (e.name === 'ValidationError') {
      return res.status(400).send(e)
    }
    return res.status(500).send(e)
  }
});

router.post('/', [auth, permit('admin', 'user'), upload.single('image')], (req, res) => {
  const artistData = req.body;

  if (req.file){
    artistData.img = req.file.filename;
  }
  const artist = new artists(artistData);
  artist.save()
    .then(result=> res.send(result))
    .catch(error => res.status(400).send(error))
});
router.get('/:id', (req, res)=>{
  artists.findById(req.params.id)
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500));
});
router.post('/:id/publish', [auth, permit('admin'), upload.single('image')], async (req, res) => {
  const artist = artists.findById(req.params.id);

  if (!artist){
    return res.sendStatus(404)
  }
  artist.publish = !artist.publish;

  await artist.save();
  return res.send(artist);

});
router.delete('/:id', [auth, permit('admin')], async (req, res)=>{
  artists.deleteOne({_id: req.params.id})
    .then(result => res.send(result))
    .catch(err => res.status(403).send(err))
});




module.exports = router;