const express = require('express');
const auth = require('../middleware/auth');
const Track_History = require('../models/TrackHisModels');
const permit = require('../middleware/permit');

const router = express.Router();

router.post('/', auth, async (req,res)=>{
  const datetime = new Date().toISOString();
  const TrackHisData = ({
    user: req.user._id,
    track: req.body.track,
    datetime
  });
  console.log(TrackHisData);
  const trackHis = new Track_History(TrackHisData);
  await trackHis.save()
    .then(result => res.send(result))
    .catch(err => res.status(403).send(err));
  res.send({Authorized: "Authorization was successful"})
});
router.get('/', [auth, permit('user', 'admin')], (req, res)=>{
  console.log(req.user);
  Track_History.find({user: req.user._id}).sort('-datetime').populate({
    path: 'track',
    populate: {path: 'album', populate: {path: 'artist'}}
  })
    .then(tracks => res.send(tracks))
    .catch(() => res.sendStatus(500))
})


module.exports = router;