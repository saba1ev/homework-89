const express = require('express');
const Tracks = require('../models/TracksModels');
const tryAuth = require('../middleware/tryAuth');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');


const router = express.Router();

router.get('/', tryAuth, async (req, res)=>{
  try {
    let criteria = {publish: true, album: req.query.album};
    if (req.user && req.user.role === 'user'){
      criteria = {
        album: req.query.album,
        $or: [
          {publish: true},
          {user: req.user._id}
        ]
      }
    }else if (req.user && req.user.role === 'admin') {
      criteria = {
        album: req.query.album
      }
    }
    const tracks = await Tracks.find(criteria).populate({path:'album', populate: {path: 'artist'}}).sort({number: 1});
    return res.send(tracks);

  } catch (e) {
    if (e.name === 'ValidationError') {
      return res.status(400).send(e)
    }
    return res.status(500).send(e)
  }
});
router.get('/:id', (req, res)=> {
  Tracks.findById(req.params.id).populate('album')
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500))
});
router.post('/', [auth, permit('user', 'admin')], (req, res)=>{
  const trackData = {...req.body, user: req.user._id};
  const Track = new Tracks(trackData);
  Track.save()
    .then(result => res.send(result))
    .catch(error => res.status(401).send(error))
});
router.post('/:id/published', [auth, permit('admin')], async (req, res)=>{
  const track = Tracks.findById(req.params.id);
  if (!track) {return res.sendStatus(404)}
  track.publish = !track.publish;
  await track.save();
  return res.send(track)
});
router.delete('/:id', [auth, permit('admin')], (req, res)=>{
  Tracks.deleteOne({_id: req.params.id})
    .then(result => res.send(result))
    .catch(err => res.status(400).send(err))
});


module.exports = router;