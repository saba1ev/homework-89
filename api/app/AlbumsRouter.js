const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');
const albums = require('../models/AlbumsModels');
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
  destination: (req, res, cb)=>{
    cb(null, config.uploadPath)
  },
  filename: (req, res, cb)=>{
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', tryAuth, async (req, res)=>{
  try {
    let criteria = {publish: true, artist: req.query.artist};

    if (req.user && req.user.role === 'user') {
      criteria = {
        artist: req.query.artist,
        $or: [
          {publish: false},
          {user: req.user._id}
        ]
      }
    } else if (req.user && req.user.role === 'admin') criteria = {
      artist: req.query.artist
    };

    const album = await albums.find(criteria).populate('artist').sort({release: 1});

    return res.send(album);
  } catch (e) {
    if (e.name === 'ValidationError') {
      return res.status(400).send(e)
    }
    return res.status(500).send(e)
  }
});

router.post('/', [auth, permit('user', 'admin'), upload.single('img')], (req, res)=>{
  const albumsData = {...req.body, user: req.user._id};
  if (req.file){
    albumsData.img = req.file.filename;
  }
  const Albums = new albums(albumsData);
  Albums.save()
    .then(result => res.send(result))
    .catch(error => res.status(401).send(error))
});
router.post('/:id/published', [auth, permit('admin')], async (req, res)=>{
  const album = albums.findById(req.params.id);
  if (!album) {return res.sendStatus(404)}
  album.publish = !album.publish;
  await album.save();
  return res.send(album)
});

router.delete('/:id', [auth, permit('admin')], (req, res)=>{
  albums.deleteOne({_id: req.params.id})
    .then(result => res.send(result))
    .catch(err => res.status(400).send(err))
});

module.exports = router;