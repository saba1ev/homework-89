const User = require('../models/UsersModels');

const tryAuth = (req, res, next) => {
  const token = req.get('Token');
  if (!token) {
    return next();
  }

  const user = User.findOne({token});
  if (!user){
    return next();
  }
  req.user = user;
  next();
};

module.exports = tryAuth;