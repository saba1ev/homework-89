const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackHisSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'Users',
    required: true
  },
  track: {
    type: Schema.Types.ObjectId,
    ref: 'Tracks',
    required: true
  },
  datetime: {
    type: String,
    required: true
  }
});

const TracksHisModels = mongoose.model('Track_History', TrackHisSchema);

module.exports = TracksHisModels;