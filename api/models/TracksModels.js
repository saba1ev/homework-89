const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TracksSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  album: {
    type: Schema.Types.ObjectId,
    ref: 'Albums',
    required: true
  },
  lasting: {
    type: String,
    required: true
  },
  publish:{
    type: Boolean,
    required: true,
    default: false
  },
  number: {
    type: Number,
    required: true
  }
});

const TracksModels = mongoose.model('Tracks', TracksSchema);

module.exports = TracksModels;