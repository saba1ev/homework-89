const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AlbumsSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  user:{
    type: Schema.Types.ObjectId,
    ref: 'Users',
    required: true
  },
  artist:{
    type: Schema.Types.ObjectId,
    ref: 'Artists',
    required: true
  },
  release: {
    type: String,
    required: true
  },
  description: String,
  publish:{
    type: Boolean,
    required: true,
    default: false
  },
  img: {
    type: String,
    required: true
  }
});

const AlbumsModels = mongoose.model('Albums', AlbumsSchema);

module.exports = AlbumsModels;