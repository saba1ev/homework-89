const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ArtistsSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  publish:{
    type: Boolean,
    required: true,
    default: false
  },
  img: {
    type: String,
    required: true
  }
});

const ArtistsModules = mongoose.model('Artists', ArtistsSchema);

module.exports = ArtistsModules;