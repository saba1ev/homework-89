const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const nanoid = require('nanoid');

const SALT_WORK_FACTOR = 10;
const Schema = mongoose.Schema;

const UsersSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async function (value) {
        if (!this.isModified('username')) return;

        const user = await UsersModels.findOne({username: value});

        if (user) throw new Error();
      },
      message: 'This username is already token'
    }
  },
  password: {
    type: String,
    required: true
  },
  nickname: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true,
    default: 'user',
    enum: ['user', 'admin']
  },
  facebookId: {
    type: String
  },
  token:{
    type: String,
    required: true
  }
});

UsersSchema.methods.checkPassword = function(password){
  return(bcrypt.compare(password, this.password))
};
UsersSchema.methods.generateToken = function(){
  return this.token = nanoid();
};
UsersSchema.pre('save', async function(next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  const hash = await bcrypt.hash(this.password, salt);

  this.password = hash;

  next();
});

UsersSchema.set('toJSON', {
  transform: (doc, ret, options) =>{
    delete ret.password;
    delete ret.token;
    delete ret._id;
    delete ret.username;
    return ret
  }
});

const UsersModels = mongoose.model('Users', UsersSchema);

module.exports = UsersModels;
