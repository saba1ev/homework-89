const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const artist = require('./app/ArtistsRouter');
const album = require('./app/AlbumsRouter');
const track = require('./app/TracksRouter');
const user = require('./app/UserRouter');
const trackHistory = require('./app/TrackHisRouter');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect(config.dbURL, config.mongoOptions).then(()=>{
  console.log('MongoDB started');
  app.use('/artist', artist);
  app.use('/album', album);
  app.use('/track', track);
  app.use('/user', user);
  app.use('/track_history', trackHistory);


  app.listen(port, ()=>{
    console.log(`Server started on ${port} port`)
  })
}).catch(()=>{
  console.log('MongoDB failed');
});